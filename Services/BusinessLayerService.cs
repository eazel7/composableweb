using System.Composition;
using Interfaces;

namespace Implementations
{
    [Export(typeof(IBusinessLayerService))]
    public class BusinessLayerService : IBusinessLayerService
    {
        private IDatabaseLayerService greetingsProvider;

        [ImportingConstructor]
        public BusinessLayerService(IDatabaseLayerService greetingsProvider)
        {
            this.greetingsProvider = greetingsProvider;
        }

        public string SayLongHi()
        {
            return "Very long " + this.greetingsProvider.SayHi();
        }
    }
}