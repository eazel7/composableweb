namespace Interfaces
{
    public interface IBusinessLayerService
    {
        string SayLongHi();
    }
}