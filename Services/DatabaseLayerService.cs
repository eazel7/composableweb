using System.Composition;
using Interfaces;

namespace Implementations
{
    [Export(typeof(IDatabaseLayerService))]
    public class DatabaseLayerService : IDatabaseLayerService
    {
        public string SayHi()
        {
            return "Hello";
        }
    }
}