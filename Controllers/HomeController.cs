﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ComposableWeb.Models;
using Interfaces;

namespace ComposableWeb.Controllers
{
    public class HomeController : Controller
    {        
        private readonly ILogger<HomeController> _logger;
        private string greetings;

        public HomeController(IBusinessLayerService service, ILogger<HomeController> logger)
        {
            _logger = logger;
            this.greetings = service.SayLongHi();
        }

        public IActionResult Index()
        {
            this.ViewData["greetings"] = this.greetings;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
